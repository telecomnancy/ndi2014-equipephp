<?php

namespace TelecomNancy\Bridge\Twig;

use Twig_Extension;
use Twig_SimpleFunction;
use Slim\Slim;

class TelecomNancyExtension extends Twig_Extension
{
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('url', array($this, 'url')),
        );
    }

    public function url($name, array $parameters=array()) {
        return $this->app->urlFor($name, $parameters);
    }

    public function getName()
    {
        return 'telecom_nancy_extension';
    }
}