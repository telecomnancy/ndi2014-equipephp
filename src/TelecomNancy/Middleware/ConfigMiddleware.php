<?php

namespace TelecomNancy\Middleware;

use Slim\Middleware;
use Datetime;

class ConfigMiddleware extends Middleware
{

    public function call()
    {
        $this->app->config = require(ROOT . '/app/config/config.php');
        $this->next->call();
    }
}