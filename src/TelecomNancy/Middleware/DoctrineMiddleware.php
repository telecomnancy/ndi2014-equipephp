<?php

namespace TelecomNancy\Middleware;

use Slim\Middleware;
use Datetime;
use TelecomNancy\Bridge\Doctrine\EntityManager;

class DoctrineMiddleware extends Middleware
{

    public function call()
    {
        $this->app->em = new EntityManager($this->app);

        $this->next->call();
    }
}