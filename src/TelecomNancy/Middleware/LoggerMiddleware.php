<?php

namespace TelecomNancy\Middleware;

use Slim\Middleware;
use Datetime;
date_default_timezone_set('Europe/Paris');
class LoggerMiddleware extends Middleware
{

    public function call()
    {
        $this->next->call();

        $datetime = new Datetime();
        $request = $this->app->request;
        $this->app->getLog()->notice(sprintf('%s [%s] %s %s %s', 
            $request->getIp(), 
            date('d/m/Y H:i:s', $datetime->getTimestamp()),
            $request->getMethod(),
            $request->getPathInfo(),
            $this->app->response->getStatus()
        ));
    }
}
