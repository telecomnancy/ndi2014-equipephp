<?php

namespace TelecomNancy\Middleware;

use Slim\Middleware;

use Twig_Loader_Filesystem;
use Twig_Environment;
use TelecomNancy\Bridge\Twig\TelecomNancyExtension;

class TwigMiddleware extends Middleware
{

    public function call()
    {
        $loader = new Twig_Loader_Filesystem(ROOT. '/src/App/Resource/views');
        $twig = new Twig_Environment($loader, array(
            'debug' => true,
            'cache' => ROOT . '/app/cache'
        ));

        $twig->addExtension(new TelecomNancyExtension($this->app));

        $this->app->templating = $twig;

        $this->next->call();
    }
}