<?php
namespace App\Entity;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="categories")
 */
class Categorie implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $image;


    public function __construct(){
       
    }

    public function __get($att){
        if(property_exists($this, $att)) return   $this -> $att;
    }
    public function __set($att, $val){
            if(property_exists($this, $att)) $this->$att = $val;
        }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image
        );

    }

}