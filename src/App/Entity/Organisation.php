<?php
namespace App\Entity;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="organisation")
 */
class Organisation extends Compte implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $intitule;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $name;


    /**
     *@var string
     *@Column(type="text")
     */
    protected $desc;

    /**
     *@var string
     *@Column(type="string", length=255)
     */
    protected $logo;

    /**
     *@var string
     *@Column(type="string", length=255)
     */
    protected $adresse;

    /**
     *@var string
     *@Column(type="string", length=255)
     */
    protected $ville;

    /**
     *@var string
     *@Column(type="string", length=255)
     */
    protected $pays;

    /**
     * @ManyToMany(targetEntity="App\Entity\Cause", inversedBy = "organisations")
     */
    protected $causes;

    public function __construct(){

    }

    public function __get($att){
        if(property_exists($this, $att)) return   $this -> $att;
    }
    public function __set($att, $val){
        if(property_exists($this, $att)) $this->$att = $val;
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude);

    }

}