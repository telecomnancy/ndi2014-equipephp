<?php
namespace App\Entity;

use Doctrine\ORM\Mapping;
/**
 * @Entity
 * @Table(name="compte")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"organisation" = "Organisation", "donneur" = "Donneur"})
 */
class Compte implements \JsonSerializable{

    /**
     * @var integer
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $email;

    /**
     * @var string
     * @Column(type="string", length=40)
     */
    protected $pw;

    public function jsonSerialize(){
        return array("id" => $this->id,"email" => $this->email, "type" => $this->type);
    }

    public function __get($att){
        if(property_exists($this, $att)) return   $this -> $att;
    }
    public function __set($att, $val){
        if(property_exists($this, $att)) $this->$att = $val;
    }
}