<?php
/**
 * Created by PhpStorm.
 * User: Léo
 * Date: 05/12/2014
 * Time: 04:37
 */

namespace App\Entity;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="trophee")
 */
class Trophee implements \JsonSerializable{

    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", length=255)
     */
    protected $nom;

        /**
         * @var string
         *
         * @Column(type="text")
         */
    protected $desc;

    /**
     * @var string
     *
     * @Column(type="string", length=255)
     */
    protected $image;

    /**
     * @OneToOne(targetEntity="App\Entity\Categorie")
     * @JoinColumn(name="categorie_id", referencedColumnName="id")
     **/
    protected $categorie;

    public function __get($att){
        if(property_exists($this, $att)) return   $this -> $att;
    }
    public function __set($att, $val){
        if(property_exists($this, $att)) $this->$att = $val;
    }

    public function jsonSerialize(){
        return array(
                    "id"=>$this->id,
                    "nom"=>$this->nom,
                    "desc"=>$this->desc,
                    "image"=>$this->image,
                    "categorie"=>$this->categorie
        );
    }

} 