<?php
namespace App\Entity;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="causes")
 */
class Cause implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @var string
     * @Column(type="text")
     */
    protected $desc;

    /**
     * @var integer
     * @Column(type="integer")
     */
    protected $obj;

    /**
     * @var integer
     * @Column(type="integer")
     */
    protected $amount;

    /**
     * @var float
     * @Column(type="float")
     */
    protected $lat;

    /**
     * @var float
     * @Column(type="float")
     */
    protected $lon;

    /**
     * @var float
     * @Column(type="date")
     */
    protected $end;

    /**
     * @ManyToOne(targetEntity="App\Entity\Categorie")
     * @JoinColumn(referencedColumnName="id")
     */
    protected $categorie;

    /**
     * @ManyToMany(targetEntity="App\Entity\Organisation", mappedBy = "causes")
     */
    protected $organisations;

    public function __construct(){

    }

    public function __get($att){
        if(property_exists($this, $att)) return   $this -> $att;
    }
    public function __set($att, $val){
        if(property_exists($this, $att)) $this->$att = $val;
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'desc' => $this->desc,
            'name' => $this->name,
            'lat' => $this->lat,
            'lon' => $this->lon,
            'categorie' => $this->categorie,
            'obj' => $this->obj,
            'amount' => $this->amount,
            'end' => $this->end->getTimestamp()
        );
    }

}