<?php
namespace App\Entity;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="donneurs")
 */

class Donneur extends Compte implements \JsonSerializable

{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", length=64)
     */
    protected $lastname;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $firstname;

    
    /**
    *@var string 
    *@Column(type="string", length=255)
    */
    protected $longitude;

    /**
    *@var string 
    *@Column(type="string", length=255)
    */
    protected $latitude;


    public function __construct(){
       
    }

    public function __get($att){
        if(property_exists($this, $att)) return   $this -> $att;
    }
    public function __set($att, $val){
            if(property_exists($this, $att)) $this->$att = $val;
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude);

    }

}