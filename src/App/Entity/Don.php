<?php
namespace App\Entity;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="don")
 */
class Don implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     * @Column(name="amount", type="float")
     */
    protected $amount;

     /**
     * @ManyToOne(targetEntity="App\Entity\Donneur")
     * @JoinColumn(name="donneur_id", referencedColumnName="id")
     **/
    protected $donneur;

     /**
     * @ManyToOne(targetEntity="App\Entity\Cause")
     * @JoinColumn(name="cause_id", referencedColumnName="id")
     **/
    protected $cause;
    
   /**
     * @var text
     * @Column(name="remerciement", type="text")
     **/
    protected $remerciement;


    public function __construct(){
       
    }

    public function __get($att){
        if(property_exists($this, $att)) return   $this -> $att;
    }
    public function __set($att, $val){
            if(property_exists($this, $att)) $this->$att = $val;
        }

    public function jsonSerialize() {
        return array();

    }

}