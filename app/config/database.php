<?php
/**
 * A copier dans le dossier app/config
 */
return array(
    'driver'    => 'pdo_mysql',
    'host'      => 'localhost',
    'dbname'    => 'nuitinfo',
    'user'      => 'root',
    'password'  => '',
    'port'      => 3306
);