var nuit = {};

nuit.markers = [];

nuit.addProjectMarkers = function () {

    /**
     * clear markers
     */
    nuit.markers.map(function (marker) {
        marker.setMap(null);
    });
    nuit.markers = [];

    /*
    $.ajax({
        url: '/causes.json',
        dataType: 'json'
    }).success(function (data) {
        console.log(data)
        data.map(function (m) {//*/
            var m = {id: 2, lat:-20, lon:130,name:"cause1", amount:6200, obj:10000, desc:"This is really a great cause because it's pretty, and it's in australia and there are koalas and kangaroos, which are pretty nice creatures."}

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(m.lat, m.lon),
                icon: '/assets/img/marker.png'
            });
            marker.setMap(nuit.map);

            google.maps.event.addListener(marker, 'click', function(e) {
                $('#currentModal .modal-name').text(m.name);
                $('#currentModal .modal-amount').text(m.amount+" $");
                $('#currentModal .modal-obj').text(m.obj+" $");
                $('#currentModal .modal-desc').text(m.desc);
                $('#currentModal .modal-help').attr('href', "/payment_"+m.id);

                var $img = $('#currentModal .modal-picture img');

                $img.attr('src','/assets/img/causes/'+m.name.toLowerCase()+'.jpg');
                $img.attr('alt', m.name.toLowerCase());

                $('#currentModal').modal('toggle');
                $('#currentModal .modal-content').bind('clickoutside',function(){
                    $('#currentModal').modal('toggle');
                });

                var $form = $('#exampleModal form');
                
                /**
                 * Submit form when clicking on send button
                 */
                $('#exampleModal #modal-submit').on('click', function (e) {
                    e.preventDefault();
                    $form.submit();
                });

                /**
                 * [description]
                 * @param  {[type]} e [description]
                 * @return {[type]}   [description]
                 */
                $form.on('submit', function (e) {
                    e.preventDefault();
                    console.log(m);
                });

                var percentage = (m.amount/m.obj)*100;
                var invertPercentage = 100-percentage;
                
                $('#currentModal .modal-picture .percentage').text(percentage+'%');
                $('#currentModal .modal-picture .overlap .alphaover').animate({
                    height:   invertPercentage+'%'
                }, 1500);
            });/*
        });
    });//*/
};

nuit.init = function () {
    nuit.map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: { lat: -25.363882, lng: 131.044922},
        zoom: 4
    });
    var styles = [
    {
    featureType: "road",
    elementType: "geometry",
    stylers: [
      { lightness: 100 },
      { visibility: "simplified" }
    ]
  },{
    featureType: "road",
    stylers: [
      { visibility: "off" }
    ]
  },{
    featureType: "landscape",
    elementType: "geometry.fill",
    stylers: [
      { color: "#efce7b"}
    ]
  },{
    featureType: "administrative",
    stylers: [
      { visibility: "off" }
    ]
  },{
    featureType: "administrative.country",
    stylers: [
      { visibility: "on" }
    ]
  },{
    featureType: "poi",
    elementType: "geometry.fill",
    stylers: [
      { color: "#efce7b" }
    ]
  }
];
/*poi   
dark brown: 4E3218;
light brown: 7f5430;
dark yellow: e9b84d;
light yellow: efce7b;
dark green: 319a47;
light green: 4bb74a;//*/

nuit.map.setOptions({styles: styles});

    google.maps.event.addListener(nuit.map, 'click', function(e) {
        nuit.map.setCenter(new google.maps.LatLng(e.latLng.k, e.latLng.B));
        nuit.map.setZoom(nuit.map.getZoom() + 1);
    });

    nuit.addProjectMarkers();
}
google.maps.event.addDomListener(window, 'load', nuit.init);