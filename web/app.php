<?php

use TelecomNancy\Bridge\Doctrine\EntityManager as EM;

define('ROOT', dirname(__DIR__));
session_start();
require ROOT . '/vendor/autoload.php';

$logHandle = fopen(ROOT . '/app/logs/slim.log', 'a');

$app = new Slim\Slim(array(
    'debug'         => true,
    'log.enabled'   => true,
    'log.level'     => Slim\Log::DEBUG,
    'log.writer'    => new Slim\LogWriter($logHandle)
));

$app->add(new TelecomNancy\Middleware\ConfigMiddleware($app));
$app->add(new TelecomNancy\Middleware\DoctrineMiddleware());
$app->add(new TelecomNancy\Middleware\TwigMiddleware());
$app->add(new TelecomNancy\Middleware\LoggerMiddleware());
$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'slim_session',
    'secret' => 'CHANGE_ME',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));

$app->get('/', function () use ($app) {
    echo $app->templating->render('index.html.twig', array(
      
    ));
})->name('index');

$app->get('/register', function () use ($app) {
    echo $app->templating->render('inscription.html.twig', array(
        
    ));
})->name('register');

$app->post('/register', function () use ($app) {

    if($app->request->post('type') == "Donnor"){
        $user = new App\Entity\Donneur();
        $user->__set('lastname', $app->request->post('user-name'));
        $user->__set('firstname', $app->request->post('user-firstname'));
        $user->__set('longitude', $app->request->post('user-long'));
        $user->__set('latitude', $app->request->post('user-lat'));
    } else {
        $user = new App\Entity\Organisation();
    }

    
    $user->__set('email', $app->request->post('user-email'));
    $user->__set('pw', hash('sha1', $app->request->post('user-password') . $app->request->post('user-name')));

    $em = new EM($app);
    $em = $em->createEntityManager();
    $em->persist($user);
    $em->flush();
    
    echo $app->templating->render('inscription.html.twig', array(
        'msg_success' => 'Inscription réussie.'
    ));
})->name('register_post');

$app->get('/payment_:id', function () use ($app) {
    echo $app->templating->render('payment.html.twig', array(
        
    ));
})->name('payment');

$app->get('/causeCreation', function () use ($app) {
    echo $app->templating->render('causeCreation.html.twig', array(
        
    ));
})->name('causeCreation');

$app->get('/causeDesc_:id', function () use ($app) {
    echo $app->templating->render('causeDesc.html.twig', array(
        
    ));
})->name('causeDesc');

$app->get('/causes.json', function () use ($app) {
    $causes = $app->em->getRepository('Cause')->findBy(array(), array('end' => 'DESC'), 20);
    echo json_encode($causes);
    return;
});


$app->get('/causes/:category', function ($category) use ($app) {
    $query = $app->em->createEntityManager()->createQuery('SELECT cau,cat from App\Entity\Cause cau LEFT JOIN cau.categorie AS cat WHERE cat.name=:category');
    $query->setParameters(array(
        'category' => $category
    ));
    $cause = $query->getResult();
    echo json_encode($causes);
    return;
});

$app->get('/aboutus', function () use ($app) {
    echo $app->templating->render('aboutus.html.twig', array(
        
    ));
})->name('aboutus');

$app->get('/homepage', function () use ($app) {
    echo $app->templating->render('homepage.html.twig', array(
        
    ));
})->name('homepage');

$app->get('/profile_:id', function () use ($app) {
    echo $app->templating->render('profile.html.twig', array(
        
    ));
})->name('profile');

$app->post('/connexion', function () use ($app) {
   
    $email = $app->request->post('email');
    $pswd = sha1($app->request->post('pswd'));

    if (!empty($email) && !empty($pswd)){
        $res = $app->em->getRepository('Compte')->findBy(array('email' => $email, 'pw' => $pswd));
        if(count($res)==1){
  /*          $_SESSION['name']=$res[0]->name;
            $_SESSION['user']=$res[0]->id;*/

            /*$session = $app->get('session');
            $session->set('name', array(
                'user' => $res[0]->name,
                'id' => $res[0]->id
            ));*/
            echo $app->templating->render('index.html.twig', array(
                'msg_success' => 'connexion réussie.',
                'username' => 'toto'
            ));
        }
    }
    echo $app->templating->render('index.html.twig', array(
       'msg_danger' => 'connexion échouée.',

    ));
});

$app->run();
fclose($logHandle);